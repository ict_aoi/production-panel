const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
.scripts([
  'resources/js/mustache.js',
  'resources/js/plugins/loaders/pace.min.js',
  'resources/js/core/libraries/jquery.min.js',
  'resources/js/core/libraries/bootstrap.min.js',
  'resources/js/plugins/loaders/blockui.min.js',
  'resources/js/core/libraries/jquery_ui/interactions.min.js',
  'resources/js/core/libraries/jquery_ui/widgets.min.js',
  'resources/js/core/libraries/jquery_ui/effects.min.js',
  'resources/js//plugins/extensions/mousewheel.min.js',
  'resources/js/plugins/forms/selects/select2.min.js',
  'resources/js/plugins/forms/styling/uniform.min.js',
  'resources/js/plugins/ui/moment/moment.min.js',
  'resources/js/plugins/ui/fullcalendar/fullcalendar.min.js',
  'resources/js/core/app.js',
  'resources/js/plugins/ui/ripple.min.js',
  'resources/js/plugins/tables/datatables/datatables.min.js',
  'resources/js/plugins/tables/datatables/extensions/responsive.min.js',
 ],'public/js/backend.js')
 .scripts([
  'resources/js/plugins/forms/tags/tagsinput.min.js',
  'resources/js/plugins/forms/tags/tokenfield.min.js',
  'resources/js/plugins/ui/prism.min.js',
  'resources/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',
  'resources/js/pages/form_tags_input.js'
],'public/js/tags.js')
 .scripts([
  'resources/js/plugins/pickers/bootstrap-datepicker.js',
  'resources/js/plugins/pickers/pickadate/picker.js',
  'resources/js/plugins/pickers/pickadate/picker.date.js',
  'resources/js/plugins/pickers/daterangepicker.js',
  'resources/js/plugins/pickers/anytime.min.js',
],'public/js/datepicker.js')
 .scripts([
  'resources/assets/js/plugins/notifications/bootbox.min.js'
], 'public/js/bootbox.js')
 .scripts([
  'resources/js/plugins/forms/styling/switch.min.js',
  'resources/js/plugins/forms/styling/switchery.min.js',
  'resources/js/plugins/forms/styling/uniform.min.js',
  'resources/js/pages/form_checkboxes_radios.js',
],'public/js/switch.js')
.scripts([
  'resources/js/pages/role.js',
],'public/js/role.js')
.scripts([
  'resources/js/pages/permission.js',
],'public/js/permission.js')
.scripts([
  'resources/js/pages/user.js',
],'public/js/user.js')
.scripts([
  'resources/js/pages/account_setting.js',
],'public/js/account_setting.js')
.scripts([
    'resources/js/pages/production.js',
    'resources/js/plugins/tables/datatables/datatables.min.js',
    'resources/js/plugins/tables/datatables/extensions/fixed_columns.min.js'
],'public/js/production.js')
.scripts([
    'resources/js/pages/production_upload_co.js',
],'public/js/production_upload_co.js')
.scripts([
    'resources/js/pages/preparation_item.js',
],'public/js/preparation_item.js')
.scripts([
    'resources/js/pages/preparation_item_show.js',
],'public/js/preparation_item_show.js')
.scripts([
    'resources/js/pages/change_over.js',
],'public/js/change_over.js')
.scripts([
    'resources/js/pages/change_over_show.js',
],'public/js/change_over_show.js')
.scripts([
    'resources/js/plugins/notifications/bootbox.min.js',
    'resources/js/plugins/notifications/sweet_alert.min.js',
    'resources/js/pages/notification.js',
],"public/js/notification.js")
.styles([
    'resources/css/bootstrap.css',
    'resources/css/core.css',
    'resources/css/components.css',
    'resources/css/colors.css',
    'resources/css/custom.css'
],'public/css/backend.css')
.copyDirectory('resources/images', 'public/images')
.copyDirectory('resources/css/icons','public/css/icons')
.sass('resources/sass/app.scss', 'public/css')
.version();
