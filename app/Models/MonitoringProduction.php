<?php

namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;


class MonitoringProduction extends Model
{


    use Uuids;
    public    $incrementing = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['plan_co_date', 'actual_co_date', 'style', 'line', 'factory', 'co_category', 'op_list', 'layout', 'fabric', 'machine', 'sample', 'man_power', 'trimcard', 'pattern', 'critical_process', 'mockup', 'created_by', 'deleted_at', 'deleted_by'];
    protected $dates        = ['created_at', 'update_at', 'deleted_at'];
}
