<?php

namespace App\Http\Controllers;


use DB;
use File;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use PHPExcel_Cell_DataType;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\MonitoringProduction;

class ProductionController extends Controller
{
    public function index(Request $request)
    {
        $factory   = auth::user()->factory_id;

        $msg = $request->session()->get('message');
        return view('production.index',compact('factory','msg'));
    }

    public function uploadCO()
    {
        $factory   = auth::user()->factory_id;


        return view('production.upload',compact('factory'));
    }

    public function data()
    {
        if(request()->ajax())
        {
            $factory   = auth::user()->factory_id;
            $data = MonitoringProduction::where('deleted_at',null)->where('factory',$factory);
            // ->orderby('created_at','desc');
            // dd($data);
            return datatables()->of($data)
            ->editColumn('uuid',function($data){
                return $data->id;
            })
            ->editColumn('factory',function($data){
                if($data->factory == '1'){
                    return 'AOI 1';
                }
                else if($data->factory == '2'){
                    return 'AOI 2';
                }
                else{
                    return 'AOI 3';
                }
            })
            ->editColumn('isactive',function($data){
                if($data->isactive == false){
                    return '<span class="label label-danger">NON ACTIVE</span>';
                }
                else{
                    return '<span class="label label-success">ACTIVE</span>';
                }
                $upload_at = Carbon::createFromFormat('Y-m-d',$data->created_at)->format('d-m-Y');
                return $upload_at;
            })
            ->addColumn('status',function($data){
                if($data->isactive == false){
                    return false;
                }
                else{
                    return true;
                }
                $upload_at = Carbon::createFromFormat('Y-m-d',$data->created_at)->format('d-m-Y');
                return $upload_at;
            })
            ->addColumn('action', function($data) {
                return view('production._action', [
                    'model'      => $data,
                    'edit_modal' => route('production.edit',$data->id),
                    'delete'     => route('production.destroy',$data->id),
                    'deactivate' => route('production.deactivate',$data->id),
                ]);
            })
            ->rawColumns(['isactive','action','status'])
            ->make(true);
        }
    }


    public function edit($id)
    {
        $production            = MonitoringProduction::find($id);
        $obj                   = new StdClass();
        $obj->id               = $id;
        $obj->plan_co_date     = $production->plan_co_date;
        $obj->actual_co_date   = $production->actual_co_date;
        $obj->style            = $production->style;
        $obj->line             = $production->line;
        $obj->factory          = $production->factory;
        $obj->category_co      = $production->co_category;
        $obj->op_list          = $production->op_list;
        $obj->layout           = $production->layout;
        $obj->fabric           = $production->fabric;
        $obj->machine          = $production->machine;
        $obj->sample           = $production->sample;
        $obj->man_power        = $production->man_power;
        $obj->trimcard         = $production->trimcard;
        $obj->pattern          = $production->pattern;
        $obj->critical_process = $production->critical_process;
        $obj->mockup           = $production->mockup;
        $obj->url_update       = route('production.update',$production->id);

		return response()->json($obj,200);
    }


    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'name' => 'required|min:3'
        // ]);

        // if(Permission::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
        //     return response()->json(['message' => 'Permission sudah ada, silahkan cari nama permission lain.'], 422);

        $production                 = MonitoringProduction::find($id);
        // $actual_co_date             = Carbon::createFromFormat('d-m-Y',$request->actual_co_date)->format('Y-m-d');
        $production->plan_co_date     = $request->plan_co_date;
        $production->actual_co_date   = $request->actual_co_date;
        $production->op_list          = $request->op_list;
        $production->layout           = $request->layout;
        $production->fabric           = $request->fabric;
        $production->machine          = $request->machine;
        $production->sample           = $request->sample;
        $production->man_power        = $request->man_power;
        $production->trimcard         = $request->trimcard;
        $production->pattern          = $request->pattern;
        $production->critical_process = $request->critical_process;
        $production->mockup           = $request->mockup;

        // $production->display_name   = $request->name;
        // $production->description    = $request->description;
        $production->save();

        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        $carbon                 = Carbon::now();
        $production             = MonitoringProduction::find($id);
        $production->deleted_at = $carbon;
        $production->deleted_by = Auth::user()->nik;
        $production->isactive   = false;
        $production->save();
        // $production = MonitoringProduction::findorFail($id)->delete();
        return response()->json(200);
    }

    public function deactivate($id)
    {
        $carbon               = Carbon::now();
        $production           = MonitoringProduction::find($id);
        $production->isactive = false;
        $production->save();
        // $production = MonitoringProduction::findorFail($id)->delete();
        return response()->json(200);
    }

    public function downloadFormUploadCO()
    {
        return Excel::create('form_upload_co',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PLAN_CO_DATE');
                $sheet->setCellValue('B1','STYLE');
                $sheet->setCellValue('C1','LINE');
                $sheet->setCellValue('D1','CO_CATEGORY');
                $sheet->setColumnFormat(array(
                    'A' => 'yyyy-mm-dd',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadFormCO(Request $request)
    {
        //return view('errors.503');
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try
                {
                    DB::beginTransaction();
                //return response()->json($data);
                    $i = 1;
                    foreach ($data as $key => $value)
                    {
                        $id           = null;                                    //$value->id;
                        $plan_co_date = trim($value->plan_co_date);
                        $style        = trim(strtoupper($value->style));
                        $line         = trim(strtoupper($value->line));
                        $co_category  = trim(strtoupper($value->co_category));
                        $factory      = Auth::user()->factory_id;

                        if($plan_co_date == null || $style == null || $line == null || $co_category == null){
                            $obj               = new stdClass();
                            $obj->id           = $id;
                            $obj->plan_co_date = $plan_co_date;
                            $obj->style        = $style;
                            $obj->line         = $line;
                            $obj->co_category  = $co_category;

                            $obj->is_error      = true;
                            $obj->status        = 'Terdapat Data Kosong';
                            $array[]           = $obj;
                        }
                        else{
                            // $carbon_co = Carbon::parse($plan_co_date)->format('Y-m-d');

                            $cek_data = MonitoringProduction::where([
                                'plan_co_date' => $plan_co_date,
                                'style'        => $style,
                                'line'         => $line,
                                'co_category'  => $co_category,
                                'factory'      => $factory,
                                'deleted_at'   => null,
                            ])
                            ->exists();

                            // dd(!$cek_data);
                            if(!$cek_data){
                                $production_monitor = MonitoringProduction::Create([
                                    'plan_co_date' => $plan_co_date,
                                    'style'        => $style,
                                    'line'         => $line,
                                    'co_category'  => $co_category,
                                    'factory'      => $factory,
                                    'created_by'   => Auth::user()->nik
                                ]);


                                $carbon = Carbon::now();
                                $obj    = new stdClass();

                                $obj->no           = $i;
                                $obj->plan_co_date = $plan_co_date;
                                $obj->style        = $style;
                                $obj->line         = $line;
                                $obj->factory      = $factory;
                                $obj->co_category  = $co_category;
                                $obj->upload_date  = Carbon::parse($carbon)->format('Y-m-d');;
                                // dd();
                                $obj->is_error     = false;
                                $obj->status       = 'success';

                                $array[]           = $obj;
                                $i++;
                            }

                        }
                    }

                    DB::commit();
                } catch (Exception $e)
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }


        }

        return response()->json($array,'200');
        /*die();
        usort($array, function($a, $b) {
            return strcmp($a->order_by,$b->order_by);
        });
        return response()->json($array,'200');*/
    }

}
