<?php namespace App\Http\Controllers;

use DB;
use File;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\Factory;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $msg = $request->session()->get('message');
        return view('user.index',compact('msg'));
    }

    public function data()
    {
        if(request()->ajax())
        {
            if(auth::user()->is_super_admin)
            {
                $data = db::select(db::raw("select users.id AS id
                ,users.name as name
                ,users.nik
                ,users.factory_id
                ,users.created_at
                from users
                where users.deleted_at is null
                order by users.created_at desc"));
            }else
            {
                $data = db::select(db::raw("select users.id AS id
                ,users.name as name
                ,users.nik
                ,users.factory_id
                ,users.created_at
                from users
                where users.deleted_at is null
                and nik != '11111111'
                order by users.created_at desc"));
            }

            return datatables()->of($data)
            ->editColumn('name',function($data){
            	return ucwords($data->name);
            })
            ->editColumn('nik',function($data){
            	return ucwords($data->nik);
            })
            ->editColumn('factory_id',function($data){
                if($data->factory_id == '1'){
                    return ucwords('AOI 1');
                }else if($data->factory_id == '2'){
                    return ucwords('AOI 2');
                }
                else{
                    return ucwords('BBIS');
                }
            })
            ->addColumn('action', function($data) {
                return view('user._action', [
                    'model'  => $data,
                    'edit'   => route('user.edit',$data->id),
                    'delete' => route('user.destroy',$data->id),
                    'reset'  => route('user.resetPassword',$data->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        }

        // $factories = Factory::pluck('name', 'id')->all();
        return view('user.create',compact('roles','factory_id'));
    }

    public function store(Request $request)
    {
        $avatar = Config::get('storage.avatar');
        // if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
        ]);

        if($request->nik)
        {
            if(User::where('nik',$request->nik)->exists())
                return response()->json('Nik sudah ada.', 422);
        }

        $image = null;
        if ($request->hasFile('photo'))
        {
            if ($request->file('photo')->isValid())
            {
                $file     = $request->file('photo');
                $now      = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image    = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height       = $image_resize->height();
                $width        = $image_resize->width();

                $newWidth  = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        // $factory = factory::find($request->factory);

        try
        {
            DB::beginTransaction();

            $user = User::firstorCreate([
                'name'              => strtolower($request->name),
                'nik'               => $request->nik,
                'factory_id'        => $request->factory,
                'email'             => $request->nik.'.dummy@'.$request->factory.'.co.id',
                'email_verified_at' => carbon::now(),
                'password'          => bcrypt('password1'),
                'is_super_admin'    => false,
                // 'create_user_id'    => auth::user()->id
            ]);

            $mappings =  json_decode($request->mappings);
            $array = array();

            foreach ($mappings as $key => $mapping) $array [] = [ 'id' => $mapping->id ];

            if($user->save()) $user->attachRoles($array);

            DB::commit();
            $request->session()->flash('message', 'success');
            return response()->json('success',200);

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }

    public function edit(Request $request,$id)
    {
        if($request->session()->has('message')) $request->session()->forget('message');

        $is_super_admin = Auth::user()->is_super_admin;
        if($is_super_admin)
        {
            $roles        = Role::pluck('display_name', 'id')->all();
            $factory_id = null;
        }else
        {
            $roles        = Role::where('name','!=','super-admin')->pluck('display_name', 'id')->all();
            $factory_id = Auth::user()->factory_id;
        }

        // $factories      = Factory::pluck('name', 'id')->all();
        $user           = User::find($id);
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role)
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        return view('user.edit',compact('roles','user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax())
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles
            join role_user on role_user.role_id = roles.id
            where role_user.user_id = '".$id."'
            "));

            return datatables()->of($data)
            ->addColumn('action', function($data)use($id){
                return view('role._action_modal', [
                    'model' => $data,
                    'delete' => route('user.destroyRoleUser',[$id,($data)?$data->id : null]),
                ]);


            })
            ->make(true);
        }

    }

    public function update(Request $request, $id)
    {
        $avatar = Config::get('storage.avatar');
        // if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $this->validate($request, [
            'name' => 'required|min:3',
        ]);

        if($request->nik)
        {
            if(User::where([
                ['nik',$request->nik],
                ['id','!=',$id],
            ])->exists())
                return response()->json('Nik sudah ada.', 422);
        }

        $user = User::find($id);
        $image = null;
        if ($request->hasFile('photo'))
        {
            if ($request->file('photo')->isValid())
            {
                $old_file = $avatar . '/' . $user->photo;
                if(File::exists($avatar)) File::delete($old_file);

                $file     = $request->file('photo');
                $now      = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image    = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height       = $image_resize->height();
                $width        = $image_resize->width();

                $newWidth  = 130;
                $newHeight = 130;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        // $factory = factory::find($request->factory);

        $user->nik          = $request->nik;
        $user->name         = strtolower($request->name);
        $user->factory_id = $request->factory;
        //if ($request->password) $user->password = bcrypt($request->password);
        if ($image) $user->photo = $image;
        $user->save();

        $request->session()->flash('message', 'success_2');
        return response()->json(200);
    }

    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->delete_at = carbon::now();
        $user->delete_user_id = auth::user()->id;
        $user->save();
        return response()->json(200);
    }

    public function resetPassword($id)
    {
        $user = User::findorFail($id);
        $user->password = bcrypt('password1');
        $user->save();
        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) {
            $array [] = $role->id;
        }

        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }

    public function getAbsence(Request $request)
    {
        $factory = $request->factory_id;
        // $factory = Factory::find($factory_id);

        if($factory)
        {
            if($factory == '1' || $factory == '2')
            {
                if($factory == '1')
                    $absences = DB::connection('absence_aoi')
                    ->table('get_employee')
                    ->select('nik','name','department_name','subdept_name')
                    ->where('factory','AOI1')
                    ->get();
                else
                    $absences = DB::connection('absence_aoi')
                    ->table('get_employee')
                    ->select('nik','name','department_name','subdept_name')
                    ->where('factory','AOI2')
                    ->get();
            }else
                $absences = DB::connection('absence_bbi')
                ->table('get_employee')
                ->select('nik','name','department_name','subdept_name')
                ->get();

            $array = [];
            foreach ($absences as $key => $value)
            {
                $array [] = $value->nik.':'.$value->name.':';
            }

            return response()->json($array,200);
        }else
        {
            return response()->json(200);
        }

    }

    public function getInformationUser(Request $request)
    {
        $nik          = $request->nik;
        $factory_id = $request->factory;

        if($factory_id)
        {
            if($factory_id == '1' || $factory_id == '2')
                $user_information = DB::connection('absence_aoi')
                ->table('get_employee')
                ->where('nik',$nik)
                ->first();
            else
                $user_information = DB::connection('absence_bbi')
                ->table('get_employee')
                ->where('nik',$nik)
                ->first();

            return response()->json($user_information,200);
        }else
        {
            return response()->json(200);
        }
    }
}
