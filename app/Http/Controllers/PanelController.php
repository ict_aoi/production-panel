<?php

namespace App\Http\Controllers;


use Auth;
use Hash;
use File;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\MonitoringProduction;

class PanelController extends Controller
{
    public function index()
    {
        $carbon  = Carbon::now();
        $date    = $carbon->format('Y-m-d');
        $date_plus = $carbon->addDays(1)->format('Y-m-d');

        return view('panel',compact('date','date_plus'));
    }
    public function dashboard_co(Request $request, $id)
    {
        $factory   = $id;
        $carbon    = Carbon::now();
        $date      = $carbon->format('Y-m-d');

        return view('change_over.index',compact('factory','date'));
    }

    public function dashboard_co_show(Request $request, $id)
    {
        $factory = $id;
        $carbon  = Carbon::now();
        $date    = $carbon->format('Y-m-d');

        return view('change_over.index_show',compact('factory','date'));
    }

    public function dataCO(Request $request)
    {
        $tanggal = $request->tanggal;
        $factory = $request->factory;


        $data = MonitoringProduction::where([['actual_co_date',$tanggal],['deleted_at',null],['factory',$factory]])
        ->orderby('line','asc')->get();

        $returnHTML = view('change_over._panel',compact('data'))->render();
        return response()->json(['html'=> $returnHTML]);
    }

    public function dashboard_preparation(Request $request, $id, $tgl)
    {
        $factory = $id;
        $carbon  = Carbon::now();

        if($tgl == null){
            $date = $carbon->format('Y-m-d');
        }
        else{
            $date = $tgl;
        }

        // return view('preparation_item.index',compact('factory','date'));
        return view('preparation_item.index',compact('factory','date'));
    }

    public function dashboard_preparation_show(Request $request, $id, $tgl)
    {
        $factory = $id;
        $carbon  = Carbon::now();

        if($tgl == null){
            $date = $carbon->format('Y-m-d');
        }
        else{
            $date = $tgl;
        }

        // return view('preparation_item.index',compact('factory','date'));
        return view('preparation_item.index_show',compact('factory','date'));
    }


    public function dataPreparation(Request $request)
    {
        //slideshow
        if(request()->ajax())
        {
            $tanggal = $request->tanggal;
            $factory = $request->factory;

            //ngerjani orang tua kampret
            $data = MonitoringProduction::
            where('plan_co_date',$tanggal)
            ->where('isactive',true)
            ->where('factory',$factory)
            ->whereNull('actual_co_date')
            ->whereNull('deleted_at')
            ->orderBy('line')
            ->get();


            // dd($data);

            $text = '<ul style="width: 100%">';
            $i=1;
            foreach ($data as $key => $value) {


                // ========= PART1 =========
                $nilai_1 = 0;
                if($value->op_list != null){
                    $nilai_1 += 1;
                }

                if($value->layout != null){
                    $nilai_1 += 1;
                }

                $hasil_1 = round(($nilai_1/2),2)*100;

                if($hasil_1 >= 99){
                    $part_1 = '<td width="10%" class="text-center bg-success" style="color:white; font-size: 15px">OK</td>';
                }
                else if($hasil_1>=66 && $hasil_1<99){
                    $part_1 = '<td width="10%" class="text-center bg-orange-400" style="color:white; font-size: 15px">ON PROCESS</td>';
                }
                else{
                    $part_1 = '<td width="10%" class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>';
                }

                // ========= PART2 =========
                $nilai_2 = 0;
                if($value->fabric != null){
                    $nilai_2 += 1;
                }

                if($value->machine != null){
                    $nilai_2 += 1;
                }

                if($value->sample != null){
                    $nilai_2 += 1;
                }

                $hasil_2 = round(($nilai_2/3),2)*100;

                if($hasil_2 >= 99){
                    $part_2 = '<td width="10%" class="text-center bg-success" style="color:white; font-size: 15px">OK</td>';
                }
                else if($hasil_2>=66 && $hasil_2<99){
                    $part_2 = '<td width="10%" class="text-center bg-orange-400" style="color:white; font-size: 15px">ON PROCESS</td>';
                }
                else{
                    $part_2 = '<td width="10%" class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>';
                }
                $part_2;

                //============ PART3 ==============
                $nilai_3 = 0;
                if($value->man_power != null){
                    $nilai_3 += 1;
                }

                $hasil_3 = ($nilai_3)*100;

                if($hasil_3 >= 99){
                    $part_3 = '<td width="10%" class="text-center bg-success" style="color:white; font-size: 15px">OK</td>';
                }
                else if($hasil_3>=66 && $hasil_3<99){
                    $part_3 = '<td width="10%" class="text-center bg-orange-400" style="color:white; font-size: 15px">ON PROCESS</td>';
                }
                else{
                    $part_3 = '<td width="10%" class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>';
                }

                //============= PART 4 ==============
                $nilai_4 = 0;
                if($value->trimcard != null){
                    $nilai_4 += 1;
                }

                if($value->pattern != null){
                    $nilai_4 += 1;
                }

                $hasil_4 = round(($nilai_4/2),2)*100;

                if($hasil_4 >= 99){
                    $part_4 = '<td width="10%" class="text-center bg-success" style="color:white; font-size: 15px">OK</td>';
                }
                else if($hasil_4>=66 && $hasil_4<99){
                    $part_4 = '<td width="10%" class="text-center bg-orange-400" style="color:white; font-size: 15px">ON PROCESS</td>';
                }
                else{
                    $part_4 = '<td width="10%" class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>';
                }

                //============= PART 5 =============
                $nilai_5 = 0;
                if($value->critical_process != null){
                    $nilai_5 += 1;
                }

                if($value->mockup != null){
                    $nilai_5 += 1;
                }

                $hasil_5 = round(($nilai_5/2),2)*100;

                if($hasil_5 >= 99){
                    $part_5 = '<td width="10%" class="text-center bg-success" style="color:white; font-size: 15px">OK</td>';
                }
                else if($hasil_5>=66 && $hasil_5<99){
                    $part_5 = '<td width="10%" class="text-center bg-orange-400" style="color:white; font-size: 15px">ON PROCESS</td>';
                }
                else{
                    $part_5 = '<td width="10%" class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>';
                }

                if($i%2==0){
                    $text .= '
                        <li>
                            <table class="table-responsive" style="width:100%">
                                <tr>
                                    <td class="text-center bg-grey-300" width="15%">
                                        <h2>LINE '.$value->line.'</h2>
                                    </td>
                                    <td class="text-center bg-grey-300" width="15%">
                                        <h2>'.$value->co_category.'</h2>
                                    </td>
                                    <td class="text-center bg-grey-300" width="20%">
                                        <h2>'.$value->style.'</h2>
                                    </td>
                                    '.$part_1.'
                                    '.$part_2.'
                                    '.$part_3.'
                                    '.$part_4.'
                                    '.$part_5.'
                                </tr>
                            </table>
                        </li>
                    ';
                }
                else{
                    $text .= '
                        <li>
                            <table class="table-responsive" style="width:100%">
                                <tr>
                                    <td class="text-center bg-grey-600" width="15%">
                                        <h2>LINE '.$value->line.'</h2>
                                    </td>
                                    <td class="text-center bg-grey-600" width="15%">
                                        <h2>'.$value->co_category.'</h2>
                                    </td>
                                    <td class="text-center bg-grey-600" width="20%">
                                        <h2>'.$value->style.'</h2>
                                    </td>
                                    '.$part_1.'
                                    '.$part_2.'
                                    '.$part_3.'
                                    '.$part_4.'
                                    '.$part_5.'
                                </tr>
                            </table>
                        </li>
                    ';
                }
                $i++;
            }


            $text .= '</ul>';
            // dd($text);
            // if($text == ''){
            //     return response()->json(200);

            // }
            // else{

                // return $text;

            $obj        = new stdClass();
            $obj->count = $i;
            $obj->text  = $text;

            return response()->json($obj,'200');
        }

        //table
        // if(request()->ajax())
        // {
        //     $tanggal = $request->tanggal;
        //     $factory = $request->factory;

        //     // dd($tanggal);

        //     $data = MonitoringProduction::where([['plan_co_date',$tanggal],['deleted_at',null],['isactive',true],['factory',$factory]])
        //     ->orderby('line','asc');

        //     return datatables()->of($data)
        //     ->editColumn('uuid',function($data){
        //         return $data->id;
        //     })
        //     ->addColumn('line', function($data) {
        //         return '<div class="panel bg-primary">
        //         <table class="table datatable-basic table-striped table-hover" >
        //             <tr style="vertical-align: center;">
        //                 <td colspan="2" style="padding: 0px;">
        //                     <h6 class="panel-title text-semibold text-center" style="font-size: 48px">'.$data->line.'</h6>
        //                 </td>
        //             </tr>
        //             <tr>
        //                 <td style="width:100%" style="padding: 5px;">
        //                     <h6 class="text-semibold text-left" style="font-size: 15px">'.$data->style.'</h6>
        //                 </td>
        //                 <td style="width:50%" style="padding: 5px;">
        //                     <p class="btn btn-warning text-semibold" style="font-size: 17px">'.$data->co_category.'</p>
        //                 </td>
        //             </tr>
        //         </table>
        //     </div>';
        //     // return 'x';
        //     })
        //     ->addColumn('part_1', function($data) {
        //         $nilai = 0;
        //         if($data->op_list != null){
        //             $nilai += 1;
        //         }

        //         if($data->layout != null){
        //             $nilai += 1;
        //         }

        //         $hasil = round(($nilai/2),2)*100;

        //         if($hasil >= 99){
        //             $echo = "OK";
        //         }
        //         else if($hasil>=66 && $hasil<99){
        //             $echo = "ON PROCESS";
        //         }
        //         else{
        //             $echo = "ALERT";
        //         }
        //         return $echo;
        //     })
        //     ->addColumn('part_2', function($data) {
        //         $nilai = 0;
        //         if($data->fabric != null){
        //             $nilai += 1;
        //         }

        //         if($data->machine != null){
        //             $nilai += 1;
        //         }

        //         if($data->sample != null){
        //             $nilai += 1;
        //         }

        //         $hasil = round(($nilai/3),2)*100;

        //         if($hasil >= 99){
        //             $echo = "OK";
        //         }
        //         else if($hasil>=66 && $hasil<99){
        //             $echo = "ON PROCESS";
        //         }
        //         else{
        //             $echo = "ALERT";
        //         }
        //         return $echo;
        //     })
        //     ->addColumn('part_3', function($data) {
        //         $nilai = 0;
        //         if($data->man_power != null){
        //             $nilai += 1;
        //         }

        //         $hasil = ($nilai)*100;

        //         if($hasil >= 99){
        //             $echo = "OK";
        //         }
        //         else if($hasil>=66 && $hasil<99){
        //             $echo = "ON PROCESS";
        //         }
        //         else{
        //             $echo = "ALERT";
        //         }
        //         return $echo;
        //     })
        //     ->addColumn('part_4', function($data) {
        //         $nilai = 0;
        //         if($data->trimcard != null){
        //             $nilai += 1;
        //         }

        //         if($data->pattern != null){
        //             $nilai += 1;
        //         }

        //         $hasil = round(($nilai/2),2)*100;

        //         if($hasil >= 99){
        //             $echo = "OK";
        //         }
        //         else if($hasil>=66 && $hasil<99){
        //             $echo = "ON PROCESS";
        //         }
        //         else{
        //             $echo = "ALERT";
        //         }
        //         return $echo;
        //     })
        //     ->addColumn('part_5', function($data) {
        //         $nilai = 0;
        //         if($data->critical_process != null){
        //             $nilai += 1;
        //         }

        //         if($data->mockup != null){
        //             $nilai += 1;
        //         }

        //         $hasil = round(($nilai/2),2)*100;

        //         if($hasil >= 99){
        //             $echo = "OK";
        //         }
        //         else if($hasil>=66 && $hasil<99){
        //             $echo = "ON PROCESS";
        //         }
        //         else{
        //             $echo = "ALERT";
        //         }
        //         return $echo;
        //     })
        //     ->rawColumns(['line','part_1','part_2','part_3','part_4','part_5'])
        //     ->make(true);
        // }
    }
}
