<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pr | Predyness</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->
        <link rel="icon" href="{{ asset('/images/logo.png')  }}" type="image/x-icon">
	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('content-css')
        <style>
            .god {
                /* min-height: 100vh;
                padding: 40px;

                display: flex;
                flex-wrap: wrap;
                justify-content: center;
                align-items: center; */

                /* background: linear-gradient(to bottom, #33ccff 27%, #ff99cc 100%);
                background-repeat: no-repeat;
                background-size: contain; */

                background:  linear-gradient(45deg, #833ab4, #ff9595  80%) no-repeat center center fixed;
                /* background: linear-gradient(-45deg, #24ff72, #9a4eff); */
                /* linear-gradient(45deg, rgba(209, 0, 42, 0.6) 0%, #0E5DC4 100%); */

                /* background: linear-gradient(to bottom left, #0066ff 27%, #ff3399 100%)no-repeat center center fixed; */
                /* background: radial-gradient(circle at bottom, #aac1c7, #f7fbfb 80%) no-repeat center center fixed; */
                background-size: cover;
                /* font-family: "Lato", "Segoe Ui", -apple-system, BlinkMacSystemFont, sans-serif; */
            }
        </style>

    </head>
    <body class="login-container god">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        @yield('page-header')
                        @yield('page-content')
                        @yield('page-modal')
                        <script src="{{ mix('js/backend.js') }}"></script>
                        <script src="{{ mix('js/datepicker.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        <script src="{{ mix('js/bootbox.js') }}"></script>
                        @yield('page-js')
                        <div class="footer text-muted">
                        {{-- &copy; 2019. <a href="route('home')">Admin - template</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
