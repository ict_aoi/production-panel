<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pr | Predyness</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->
        <link rel="icon" href="{{ asset('/images/logo.png')  }}" type="image/x-icon">
	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">
        @yield('content-css')

    </head>
    <body class="login-container wa-cover">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        @yield('page-header')
                        @yield('page-content')
                        @yield('page-modal')
                        <script src="{{ mix('js/backend.js') }}"></script>
                        <script src="{{ mix('js/datepicker.js') }}"></script>
                        <script src="{{ mix('js/notification.js') }}"></script>
                        <script src="{{ mix('js/bootbox.js') }}"></script>
                        @yield('page-js')
                        <div class="footer text-muted">
                        {{-- &copy; 2019. <a href="route('home')">Admin - template</a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
