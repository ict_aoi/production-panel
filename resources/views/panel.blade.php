@extends('layouts.app_panel',['active' => 'production_panel'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content text-center">
        {{-- <div class="page-title text-center"> --}}
            <h1 style="font-size: 45px"><i class="icon-grid5 position-center"></i> <span class="text-center text-bold">PRODUCTION PANEL</span> <i class="icon-grid5 position-center"></i></h1>
            {{-- <br> --}}
            <a class="btn bg-pink-300" href="{{ route('login') }}"><span>LOGIN</span></a>
            <br><br><br>
        {{-- </div> --}}
    </div>
</div>
@endsection
@section('page-content')
    {{-- <br>
    <br> --}}
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <div class="panel panel-primary panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title">Change Over</h6>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>

                <div class="panel-body">
                    <p class="justify"> Dashboard change over tiap line, berdasarkan upload excel dari admin</p><br><br>
                    <a href="{{ URL('/dashboard-co/1') }}" class="btn btn-default col-xs-6">AOI 1</a>
                    <a href="{{ route('dashboard-co-show',['1',$date]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                    <br><br><br>
                    <a href="{{ route('dashboard-co','2') }}" class="btn btn-default col-xs-6">AOI 2</a>
                    <a href="{{ route('dashboard-co-show',['2',$date]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                    <br><br><br>
                    <a href="{{ route('dashboard-co','3') }}" class="btn btn-default col-xs-6">AOI 3</a>
                    <a href="{{ route('dashboard-co-show',['3',$date]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-primary panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title">Preparation Item</h6>
                    {{-- <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div> --}}
                </div>

                <div class="panel-body justify">
                    <p class="justify"> Dashboard preparation item tiap line, berdasarkan inputan admin dari sistem</p><br><br>
                    <a href="{{ URL('/dashboard-preparation/1/'.$date_plus) }}" class="btn btn-default col-xs-6">AOI 1</a>
                    <a href="{{ route('dashboard-preparation-show',['1',$date_plus]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                    <br><br><br>
                    <a href="{{ route('dashboard-preparation',['2',$date_plus]) }}" class="btn btn-default col-xs-6">AOI 2</a>
                    <a href="{{ route('dashboard-preparation-show',['2',$date_plus]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                    <br><br><br>
                    <a href="{{ route('dashboard-preparation',['3',$date_plus]) }}" class="btn btn-default col-xs-6">AOI 3</a>
                    <a href="{{ route('dashboard-preparation-show',['3',$date_plus]) }}" class="btn bg-blue-600 col-xs-6">SHOW</a>
                </div>
            </div>
        </div>

        <div class="col-md-3"></div>
        {{-- <div class="col-md-3">
            <div class="panel panel-primary panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title">Primary panel</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Primary bordered panel using <code>.panel-bordered</code> class
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-primary panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title">Primary panel</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    Primary bordered panel using <code>.panel-bordered</code> class
                </div>
            </div>
        </div> --}}
    </div>


    {{-- </div> --}}
    {{-- <div class="col-md-3">
        <div class="panel panel-primary panel-bordered">
            <div class="panel-heading">
                <h6 class="panel-title">Primary panel</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                Primary bordered panel using <code>.panel-bordered</code> class
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-primary panel-bordered">
            <div class="panel-heading">
                <h6 class="panel-title">Primary panel</h6>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                Primary bordered panel using <code>.panel-bordered</code> class
            </div>
        </div>
    </div> --}}

    {{-- <div class="row">
        <div class="col-md-3">
            <div class="panel bg-primary">
            </div>
        </div>
    </div> --}}
    <!-- /solid color panels -->
@endsection

@section('page-js')

    <script>
        $('#tanggalCO').click(function(){
            var tgl = $('#tanggalCO').val();

            var tgl = $('#tanggalCO').val();
            $('#up_actual_co_date').val(tgl);
            $('#updateTanggalModal').modal();
        });

        $(document).ready( function ()
        {

            var date_co = $('#_date').val();
            $('#tanggalCO').val(date_co);
            $('input.input-date').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight: true
            });

            var factory =  $('#_factory').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: '/dashboard-co/'+factory+'/data',
                data: {
                    tanggal: date_co,
                    factory: factory
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    // $('#updateTanggalModal').modal('hide');
                    // $('#update_date_co').trigger("reset");
                    // console.log(date_co);
                   //reload data panel ajax here
                    $('.panel-co').html(response.html)
                    // $('#productionTable').DataTable().ajax.reload();
                    // $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    // $('#updateTanggalModal').modal();
                }
            });
        });

        $('#update_date_co').submit(function (event){
            event.preventDefault();

            var factory = $('#_factory').val();
            var up_date    = $('#up_actual_co_date').val();

            $.ajax({
                type: "post",
                url: '/dashboard-co/'+factory+'/data',
                data: {
                    tanggal: up_date,
                    factory: factory
                },
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    // $('#updateTanggalModal').modal('hide');
                    // $('#update_date_co').trigger("reset");

                    $('#updateTanggalModal').modal('hide');
                    $('#tanggalCO').val(up_date);

                    // $("#alert_success").trigger("click", 'Data Yang Tampil Berdasar Tanggal Pilihan');
                    $('#update_date_co').trigger("reset");
                   //reload data panel ajax here
                    $('.panel-co').html(response.html)

                },
                error: function (response) {
                    $.unblockUI();
                    if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                    $('#updateTanggalModal').modal();
                }
            });
        });
    </script>
@endsection

