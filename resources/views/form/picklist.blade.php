<div class="form-group col-lg-12 {{ isset($div_class) ? $div_class : '' }}" id="{{ isset($div_id) ? $div_id : ''  }}">
@if (isset($label))
<label class="control-label text-semibold {{ isset($label_col) ? $label_col : 'col-lg-2' }}">{{ $label }}</label>
@endif

<div id="{{ $field }}_error" class="input-group {{ $errors->has($field) ? 'has-error' : '' }} {{ isset($form_col) ? $form_col : 'col-lg-10' }}">
	
	{!! Form::text($field, isset($default) ? $default : null, [
			'id' => $name . 'Name',
			'class' => 'form-control',
			'readonly' => (isset($readonly))?'readonly':NULL,
			'placeholder' => $placeholder ? $placeholder : '',
			'autocomplete'=>'off'
			#'data-toggle' => 'modal',
			##'data-target' => '#' . $name . 'Modal'
		]) 
	!!}
	
	@if (isset($delete) && $delete)
	<div class="input-group-btn">
		<button class="btn btn-default" type="button" id="{{ $name }}ButtonDel" title="Hapus">
			<span class="text-danger">
				<span class="glyphicon glyphicon-remove"></span>
			</span>
		</button>
	</div>
	@endif
	<span class="input-group-btn">
		<button class="btn btn-default legitRipple" type="button" id="{{ $name }}ButtonLookup">
			<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
		</button>
	</span>
</div>

<div class="has-error">
	@if ($errors->has($field))
	<span class="help-block text-danger">{{ $errors->first($field) }}</span>
	@endif
</div>

@if (isset($mandatory))
	<span id="{{ $field }}_danger" class="help-block text-danger">{{ $mandatory }}</span>
@endif

@if (isset($help))
	<span class="help-block">{{ $help }}</span>
@endif

{!! Form::hidden($field . '_id', isset($hidden_value) ? $hidden_value : null, ['id' => $name . 'Id']) !!}
</div>