@extends('layouts.app',['active' => 'user'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">User</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Account Management</li>
            <li><a href="{{ route('user.index') }}">User</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-body">
                {!!
                    Form::open([
                        'role'    => 'form',
                        'url'     => route('user.update',$user->id),
                        'method'  => 'post',
                        'class'   => 'form-horizontal',
                        'enctype' => 'multipart/form-data',
                        'id'      => 'form'
                    ])
                !!}

                    @include('form.select', [
                        'field'     => 'factory',
                        'label'     => 'Pabrik',
                        'default'   => $user->factory_id,
                        'mandatory' => '*Wajib diisi',
                        'options'   => [
                            ''  => '-- Pilih Jenis Pabrik --',
                            '1' => 'AOI 1',
                            '2' => 'AOI 2',
                            '3' => 'BBIS',
                ],
                        'div_class'  => (auth::user()->is_super_admin)? '':'hidden',
                        'class'      => 'select-search',
                        'label_col'  => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col'   => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'factory'
                        ]
                    ])

                    <div class="form-group  col-lg-12">
                            <label for="nik" class="text-semibold control-label col-md-2 col-lg-2 col-sm-12">
                            Nik
                        </label>
                        <div class="control-input col-md-10 col-lg-10 col-sm-12">
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" id="nik" name="nik" value="{{ $user->nik }}" readonly="readonly">
                                <span  class="help-block text-danger">*Wajib diisi</span>
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-base"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    @include('form.text', [
                        'field' => 'name',
                        'default' => $user->name,
                        'label' => 'Nama',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'name',
                            'readonly' => 'readonly'
                        ]
                    ])

                    @include('form.select', [
                        'field' => 'role',
                        'label' => 'Kelompok Akses',
                        'options' => [
                            '' => '-- Pilih Kelompok Akses --',
                        ]+$roles,
                        'class' => 'select-search',
                        'label_col' => 'col-md-2 col-lg-2 col-sm-12',
                        'form_col' => 'col-md-10 col-lg-10 col-sm-12',
                        'attributes' => [
                            'id' => 'select_role'
                        ]
                    ])

                    {!! Form::hidden('user_id', $user->id, array('id' => 'user_id')) !!}
                    {!! Form::hidden('form_status', 'edit', array('id' => 'form_status')) !!}
                    {!! Form::hidden('auto_completes', '[]', array('id' => 'auto_completes')) !!}
                    {!! Form::hidden('mappings', json_encode($mappings), array('id' => 'mappings')) !!}
                <div class="text-right">
                    <button type="submit" class="btn btn-primary legitRipple">Simpan <i class="icon-arrow-right14 position-right"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title text-semibold">Pemetaan Pengguna Dengan Kelompok Akses &nbsp;</span></h6>
            </div>
            <div class="panel-body">
                <table class="table datatable-basic table-striped table-hover" id="roleTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kelompok Akses</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
{!! Form::hidden('url_data_role',route('user.dataRole',$user->id), array('id' => 'url_data_role')) !!}
@endsection

@section('page-js')
<script src="{{ mix('js/switch.js') }}"></script>
<script src="{{ mix('js/user.js') }}"></script>
@endsection
