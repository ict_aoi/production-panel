@extends('layouts.app',['active' => 'co'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Change Over</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            {{-- <li >Change Over</li> --}}
            <li class="active">Change Over</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a href="{{ route('production.uploadCO') }}" class="btn btn-default" ><i class=" icon-file-upload2 position-left"></i>Upload</a>
			</div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic" id="productionTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id</th>
                        <th>Status</th>
                        <th>Tanggal Plan CO</th>
                        <th>Tanggal Actual CO</th>
                        <th>Style</th>
                        <th>Line</th>
                        <th>Factory</th>
                        <th>Kategori CO</th>
                        <th>OP List</th>
                        <th>Layout</th>
                        <th>Fabric</th>
                        <th>Machine</th>
                        <th>Sample</th>
                        <th>Man Power</th>
                        <th>Trimcard</th>
                        <th>Pattern</th>
                        <th>Critical Process</th>
                        <th>Mockup</th>
                        <th>Tanggal Upload</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

{!! Form::hidden('form_status', 'index', array('id' => 'form_status')) !!}
{!! Form::hidden('mappings', '[]', array('id' => 'mappings')) !!}
{!! Form::hidden('msg', $msg, array('id' => 'msg')) !!}
@endsection


@section('page-modal')
	@include('production._update_modal')
@endsection

@section('page-js')

{{-- <script src="https://cdn.datatables.net/fixedcolumns/3.2.4/js/dataTables.fixedColumns.min.js"></script> --}}
<script src="{{ mix('js/production.js') }}"></script>
@endsection
