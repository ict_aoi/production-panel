@extends('layouts.app',['active' => 'co'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Change Over</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li ><a href="{{ route('production.index') }}">Change Over </a></li>
            <li class="active">Upload</li>
        </ul>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-default border-grey">
    <div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			{{-- <div class="heading-elements">
				<a href="{{ route('production.downloadFormUploadCO') }}" class="btn btn-default" ><i class="icon-plus2 position-left"></i>Download Form Upload</a>
            </div> --}}

            <div class="heading-elements">
                <a class="btn btn-primary btn-icon" href="{{ route('production.downloadFormUploadCO')}}" data-popup="tooltip" title="Download Form CO" data-placement="bottom" data-original-title="download form co"><i class="icon-download"></i></a>
                <button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="Upload Form CO" data-placement="bottom" data-original-title="upload form co"><i class="icon-upload"></i></button>

                {!!
                    Form::open([
                        'role'    => 'form',
                        'url'     => route('production.uploadFormCO'),
                        'method'  => 'POST',
                        'id'      => 'upload_file_co',
                        'enctype' => 'multipart/form-data'
                    ])
                !!}
                    <input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    <button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
                {!! Form::close() !!}
            </div>
		</div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table datatable-basic table-striped table-hover" id="uploadCOTable">
                <thead>
                    <tr>
                        <th>No</th>
                        {{-- <th>Id</th> --}}
                        <th>Tanggal Plan CO</th>
                        <th>Style</th>
                        <th>Line</th>
                        <th>Factory</th>
                        <th>Kategori CO</th>
                        <th>Tanggal Upload</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="tbody-co">
                </tbody>
            </table>
        </div>
        {!! Form::hidden('page','import', array('id' => 'page')) !!}
        {!! Form::hidden('list','[]',array('id' => 'list')) !!}
    </div>
</div>
@endsection

@section('page-js')
    {{-- <script src="{{ mix('js/role.js') }}"></script> --}}

    @include('production._item_production_co');
    <script src="{{ mix('js/production_upload_co.js') }}"></script>
@endsection
