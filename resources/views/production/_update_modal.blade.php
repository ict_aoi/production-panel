<div id="updateProductionModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role'   => 'form',
						'url'    => '#',
						'method' => 'post',
						'class'  => 'form-horizontal',
						'id'     => 'update_production'
					])
				!!}
				<div class="modal-body">

                    <div class="row">
                        <div class="col-md-6">
                            {{-- @include('form.date', [
                                'field' 		=> 'plan_co_date',
                                'label' 		=> 'Tanggal Plan CO',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                // 'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_plan_co_date',
                                    'readonly' 		=> 'readonly',
                                ]
                            ]) --}}
                            {{-- @include('form.text', [
                                'field'      => 'plan_co_date',
                                'label'      => 'Tanggal Plan CO',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'attributes' => [
                                    'id'       => 'up_plan_co_date',
                                    'readonly' => 'readonly'
                                ]
                            ]) --}}

                            @include('form.date', [
                                'field' 		=> 'plan_co_date',
                                'label' 		=> 'Tanggal Plan CO',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_plan_co_date',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])

                            @include('form.text', [
                                'field'      => 'style',
                                'label'      => 'Style',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'attributes' => [
                                    'id'       => 'up_style',
                                    'readonly' => 'readonly'
                                ]
                            ])

                            @include('form.text', [
                                'field'      => 'category_co',
                                'label'      => 'Kategori CO',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'attributes' => [
                                    'id'       => 'up_category_co',
                                    'readonly' => 'readonly'
                                ]
                            ])
                        </div>
                        <div class="col-md-6">
                            @include('form.date', [
                                'field' 		=> 'actual_co_date',
                                'label' 		=> 'Tanggal Actual CO',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_actual_co_date',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])

                            @include('form.text', [
                                'field'      => 'line',
                                'label'      => 'Line',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'attributes' => [
                                    'id'       => 'up_line',
                                    'readonly' => 'readonly'
                                ]
                            ])

                            @include('form.text', [
                                'field'      => 'factory',
                                'label'      => 'Factory',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'attributes' => [
                                    'id'       => 'up_factory',
                                    'readonly' => 'readonly'
                                ]
                            ])
                        </div>
                    </div>
                <br>

                    <div class="row">
                        <div class="col-md-6">
                            @include('form.date', [
                                'field' 		=> 'op_list',
                                'label' 		=> 'OP List',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_op_list',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'layout',
                                'label' 		=> 'Layout',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_layout',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'fabric',
                                'label' 		=> 'Fabric',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_fabric',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'machine',
                                'label' 		=> 'Machine',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_machine',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'sample',
                                'label' 		=> 'Sample',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_sample',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                        </div>
                        <div class="col-md-6">
                            @include('form.date', [
                                'field' 		=> 'man_power',
                                'label' 		=> 'Man Power',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_man_power',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'trimcard',
                                'label' 		=> 'Trimcard',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_trimcard',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'pattern',
                                'label' 		=> 'Pattern',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_pattern',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'critical_process',
                                'label' 		=> 'Critical Process',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_critical_process',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                            @include('form.date', [
                                'field' 		=> 'mockup',
                                'label' 		=> 'Mockup',
                                'label_col'     => 'col-md-4 col-lg-4 col-sm-12',
                                'form_col'      => 'col-md-8 col-lg-8 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_mockup',
                                    'readonly' 		=> 'readonly',
                                    'autocomplete' 	=> 'off'
                                ]
                            ])
                        </div>
                    </div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit Form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
