@extends('layouts.app', ['active' => 'dashboard'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Account Setting</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('home') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Account Setting</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-flat">
        <div class="panel-body">
            {!!
                Form::open([
                    'role'      => 'form',
                    'url'       => route('accountSetting.updateAccount',$obj->id),
                    'method'    => 'post',
                    'enctype'   => 'multipart/form-data',
                    'id'        => 'form'
                ])
            !!}
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Nama</label>
                            <input type="text" value="{{ $obj->name }}" name="name" id="name" class="form-control">
                        </div>

                        <div class="col-md-6">
                            <label>Email</label>
                            <input type="text" value="{{ $obj->email }}" name="email" id="email" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Factory</label>
                            <input type="text" value="{{ $obj->factory_name }}" name="factory" id="factory" readonly="readonly" class="form-control">
                        </div>

                        <div class="col-md-6">
                            <label>Departemen</label>
                            <input type="text" value="{{ $obj->department }}" name="department" id="department" readonly="readonly" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Old Password</label>
                            <input type="password" name="old_password" placeholder="Enter Old password" class="form-control">
                            @if ($errors->has('old_password'))
                                <span class="help-block" style="color: red;">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="col-md-6">
                            <label>New Password</label>
                            <input type="password" name="new_password" placeholder="Repeat new password" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">

                        </div>
                        <div class="col-md-6">
                            {{-- <label class="display-block">Upload profile image</label>
                            <input type="file" class="file-styled" id="photo" name="photo" accept="image/*">
                            <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span> --}}
                            <label>Retype Password</label>
                            <input type="password" name="retype_password" placeholder="Repeat new password" class="form-control">
                            @if ($errors->has('retype_password'))
                                <span class="help-block" style="color: red;">
                                    <strong>{{ $errors->first('retype_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="text-right">
                <button type="submit" class="btn btn-blue-success col-xs-12 legitRipple">Save <i class="icon-floppy-disk position-right"></i></button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection


@section('page-js')
    <script src="{{ mix('js/account_setting.js') }}"></script>
@endsection
