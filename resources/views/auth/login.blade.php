<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Pr | Predyness</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <!-- Global stylesheets -->
        <link rel="icon" href="{{ asset('/images/logo.png')  }}" type="image/x-icon">
	    <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	    <link href="{{ mix('css/backend.css') }}" rel="stylesheet" type="text/css">

    </head>
    <body class="login-container login-cover">
        <div class="page-container">
            <div class="page-content">
                <div class="content-wrapper">
                    <div class="content">
                        <form  method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <a href="#"><img src="{{ asset('images/logo2.png') }}" style="width:100px;height:100px;" class="img-circle  " alt="logo_predyness"></a>

                                    {{-- <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div> --}}
                                    <h5 class="content-group-lg">Login to your account <small class="display-block">Enter your credentials</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" class="form-control {{ $errors->has('nik') || $errors->has('email') ? 'has-error' : '' }}" value="{{ old('nik') ?: old('email') }}" placeholder="NIK / E-mail" name="login" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" class="form-control" placeholder="Password" name="password" required>
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group login-options">
                                    <div class="row">
                                        <div class="col-sm-6 text-right">
                                            <a href="{{ route('password.request') }}">Forgot password?</a>
                                        </div>
                                    </div>
                                </div>

                                @if ($errors->has('employee_code'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('employee_code') }}</strong>
                                    </span>
                                @elseif ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @elseif (session()->has('flash_notification.message'))
                                    <span class="help-block text-danger">
                                        <strong>{!! session()->get('flash_notification.message') !!}</strong>
                                    </span>
                                @endif
                                <div class="form-group">
                                    <button type="submit" class="btn bg-pink-400 btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>
