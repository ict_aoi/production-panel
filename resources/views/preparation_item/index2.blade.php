@extends('layouts.app_dashboard',['active' => 'production_panel'])

@section('content-css')
<style>
    body,
    html {
        margin: 0;
        height: 100%;
        overflow: hidden;
    }

    .wrap {
        width: 100%;
        height: 100vh;
        position: relative;
        overflow: hidden;
    }

    .html::-webkit-scrollbar {
        display: none;
    }

    .slide-container {
        position: absolute;
        left: 0;
        height: 100%;
    }

    .slides {
        display: flex;
        flex: row;
        flex-wrap: no-wrap;
        height: 100%;
        margin: 0;
        padding: 0;
    }

    .animate {
        animation: moveSlideshow 20s linear infinite;
    }

    @keyframes moveSlideshow {
        100% {
            transform: translate3d(calc(-100% + 100vw), 0, 0);
        }
    }


    .slides li {
        display: block;
        box-sizing: border-box;
        width: 33.33vw;
        height: 100%;
        align-items: center;
        justify-content: center;
        text-align: center;
    }

    .slides li>div {
        display: flex;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        /* border-radius: 30px; */
        /* font-family: 'Roboto', sans-serif;
        font-weight: bold; */
        text-align: center;
        justify-content: center;
        align-self: center;
    }

    .slides li span {
        display: block;
        /* font-size: 40vh; */
        /* line-height: 100vh; */
        /* font-weight: 800; */
        /* letter-spacing: -.06em; */
        color: #00509A;
    }

    section{
        width: 100%;
        height: 100vh;
        box-sizing: border-box;
        padding: 50px 0;
    }

    .card{
        position: relative;
        max-width: 300px;
        height: 500px;
        border: none;
        margin: 0 auto;
        margin-bottom: 15px;
        padding: 40px 20px;
        box-shadow: 0 10px 15px rgba(0,0,0,.15);
        transition: 5s;
        overflow: hidden;
        transform: scale(1);
        transition: all .35s;
        background-color: #33333a;
    }

    .card:hover{
        transform: scale(1.05);
        transition: all .35s;
    }

    .card,
    .card .title .fas{


        /* background: linear-gradient(-45deg, #f403d1, #645bf6); */
        background-color: #33333a;
        -webkit-border-radius: 15px 15px 15px 15px;
        border-radius: 15px 15px 15px 15px;

    }

    /* .card,
    .card .title .fas{

        background: linear-gradient(-45deg, #ffec61, #f321d7);
        -webkit-border-radius: 15px 15px 15px 15px;
        border-radius: 15px 15px 15px 15px;
    } */

    /* .card,
    .card .title .fas{

        background: linear-gradient(-45deg, #24ff72, #9a4eff);
        -webkit-border-radius: 15px 15px 15px 15px;
        border-radius: 15px 15px 15px 15px;
    } */

    .card:before{
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 40%;
        background: rgba(255, 255,255, .1);
        z-index: 1;
        transform: skewY(-5deg) scale(1.5);
    }

    .title .fas{
        color: #fff;
        font-size: 60px;
        width: 100px;
        height: 100px;
        border-radius: 50%!important;
        text-align: center;
        line-height: 100px;
        box-shadow: 0 10px 10px rgba(0,0,0,.15);
    }

    .title h2{
        position: relative;
        margin: 20px 0 0;
        padding: 0;
        color: #fff;
        font-size: 28px;
        z-index: 2;
    }

    .price{
        /* position: relative; */
        z-index: 2;
    }

    .price {
        margin: 0;
        padding: 20px 0;
        color: #fff;
        /* font-size: 60px; */
    }

    .option{
        position: relative;
        z-index: 2;
    }

    .option ul{
        margin: 0;
        padding: 0;
    }

    .option ul li{
        margin: 0 0 10px;
        padding: 0;
        list-style: none;
        color: #fff;
        font-size: 16px;
    }

    .card a{
        position: relative;
        z-index: 2;
        background: #fff;
        color: slategray;
        width: 150px;
        height :40px;
        line-height: 40px;
        border-radius: 40px;
        display: block;
        text-align: center;
        margin: 20px auto 0;
        font-size: 16px;
        cursor: pointer;
        box-shadow: 0 5px 10px rgba(0,0,0,.15);
        transform: scale(1);
        transition: all .35s;
    }

    .card a:hover{
        text-decoration: none;
        transform: scale(1.05);
        transition: all .35s;
    }

    .fa-plane{
        transform: rotate(-45deg);
    }

    .fa-check{
        padding-right: 10px;
    }

    .fa-times{
        padding-right: 10px;
    }

</style>

@endsection
@section('page-header')
<div class="page-header">
    <div class="page-header-content text-center">
        {{-- <div class="page-title text-center"> --}}
        <h1 style="font-size: 45px"><i class="icon-grid5 position-center"></i> <span
                class="text-center text-bold">DASHBOARD PREPARATION ITEM</span> <i
                class="icon-grid5 position-center"></i></h1>
        {{-- <br> --}}

        {!! Form::hidden('_factory', $factory,array('id' => '_factory')) !!}
        {!! Form::hidden('_date', $date,array('id' => '_date')) !!}
        {{-- <button type="button" onclick="tanggal()" class="btn btn-default col-xs-12" value="XXX"> </button> --}}
        <input type="text" id="tanggalPreparation" class="btn btn-default col-xs-12" readonly>
        {{-- <br> --}}
        {{-- </div> --}}
    </div>
</div>
@endsection
@section('page-content')
{{-- <br>
<br> --}}
{{-- <div class="table-responsive">
        <table class="table datatable-basic text-center" id="preparation_table">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center bg-grey">Line</th>
                    <th class="text-center bg-grey">OP List & Layout</th>
                    <th class="text-center bg-grey">Fabric, Machine & Sample</th>
                    <th class="text-center bg-grey">Man Power</th>
                    <th class="text-center bg-grey">Trimcard & Pattern</th>
                    <th class="text-center bg-grey">Critical Process & Mockup</th>
                </tr>
            </thead>
        </table> --}}
{{-- </div> --}}
<div class="wrap">
    <div class="slide-container">
        <ul class="slides">
            {{-- <div id="data-co">
            </div> --}}
            {{-- <li></li> --}}

            {{-- <li></li> --}}
        </ul>
    </div>
</div>

<!-- /solid color panels -->
@endsection


@section('page-modal')
@include('preparation_item._tanggal_modal')
@endsection

@section('page-js')
<script src="{{ mix('js/preparation_item.js') }}"></script>
<script>
    // $(document).ready(function () {
    //     $slides = $('.slides');
    //     $slides.bind('contentchanged', function () {
    //         animate($slides);
    //     });
    //     animate($slides);
    // });

    // function animate($slides) {
    //     var slidesLength = $slides.find('li').length;
    //     // console.log($slides.find('li:nth-last-child(-n+3)').clone().prependTo($slides));
    //     if (slidesLength > 3) {
    //         $slides.find('li:nth-last-child(-n+3)').clone().prependTo($slides);
    //         $slides.addClass('animate');
    //         $slides.css('animation-duration', (slidesLength+1) * 2 + 's');
    //     }
    // }
</script>
@endsection
