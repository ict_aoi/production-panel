<div class="row">
    @foreach ($data as $value)

    {{-- @php
        // foreach($data as $value):
        // echo $data[0]['line'];
        echo "line";
    @endphp --}}
    <div class="col-md-3">
        <div class="panel bg-primary">
            <table class="table datatable-basic table-striped table-hover" >
                <tr style="vertical-align: center;">
                    <td colspan="2" style="padding: 0px;">
                        <h6 class="panel-title text-semibold text-center" style="font-size: 48px">{{$value->line}}</h6>
                    </td>
                </tr>
                <tr>
                    <td style="width:100%" style="padding: 5px;">
                        <h6 class="text-semibold text-left" style="font-size: 15px">{{$value->style}}</h6>
                    </td>
                    <td style="width:50%" style="padding: 5px;">
                        <p class="btn btn-warning text-semibold" style="font-size: 17px">{{$value->co_category}}</p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    {{-- @php
        // endforeach
        echo "babe";
    @endphp --}}
    @endforeach
</div>
