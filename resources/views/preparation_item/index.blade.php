@extends('layouts.app_dashboard',['active' => 'production_panel'])

@section('content-css')
<style>
    body{
        overflow: hidden;
    }

    .login-container .page-container {
        padding-top: 10px;
        position: static;
    }

    #news-container
    {
        width: 1305px;
        height: auto;
        margin: auto;
        margin-top: 10px;
        border: 5px solid #333333;
    }

    #news-container ul li div
    {
        border: 1px solid #aaaaaa;
        /* background: #ffffff; */
    }
</style>

@endsection
@section('page-header')
<div class="page-header">
    <div class="page-header-content text-center">
        {{-- <div class="page-title text-center"> --}}
        <h1 style="font-size: 45px"><i class="icon-grid5 position-center"></i> <span
                class="text-center text-bold">DASHBOARD PREPARATION ITEM</span> <i
                class="icon-grid5 position-center"></i></h1>
        {{-- <br> --}}

        {!! Form::hidden('_factory', $factory,array('id' => '_factory')) !!}
        {!! Form::hidden('_date', $date,array('id' => '_date')) !!}
        {{-- <button type="button" onclick="tanggal()" class="btn btn-default col-xs-12" value="XXX"> </button> --}}
        <input type="text" id="tanggalPreparation" class="btn btn-default col-xs-12" readonly>
        {{-- <br> --}}
        {{-- </div> --}}
    </div>
</div>
@endsection
@section('page-content')
{{-- <br>
<br> --}}
{{-- <div class="table-responsive">
         <div>
             <table class="table datatable-basic text-center" id="preparation_table">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center bg-grey">Line</th>
                    <th class="text-center bg-grey">OP List & Layout</th>
                    <th class="text-center bg-grey">Fabric, Machine & Sample</th>
                    <th class="text-center bg-grey">Man Power</th>
                    <th class="text-center bg-grey">Trimcard & Pattern</th>
                    <th class="text-center bg-grey">Critical Process & Mockup</th>
                </tr>
            </thead>
        </div>
        </table> --}}
{{-- </div> --}}
<br><br><br>
<div class="row" style="width:1200px; margin: auto;">
    <table class="table-responsive" style="width:1200px; border: 5px solid #333333;">
        <tr>
            <td class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px" width="15%">
                LINE
            </td>
            <td class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px" width="15%">
                CATEGORY CO
            </td>
            <td class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px" width="20%">
                STYLE
            </td>
            <td width="10%" class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px">OP List &<br> Layout</td>
            <td width="10%" class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px">Fabric, Machine & Sample</td>
            <td width="10%" class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px">Man Power</td>
            <td width="10%" class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px">Trimcard & Pattern</td>
            <td width="10%" class="text-center text-bold bg-slate-800" style="color:white; font-size: 15px">Critical Process & Mockup</td>
        </tr>
    </table>
</div>
<div id="news-container">
    {{-- <ul>
        <li>
            <div >
                <table class="table-responsive" width="1200px">
                    <tr>
                        <td  <td class="bg-grey-300" width="15%">
                            <h2>LINE 1</h2>
                        </td>
                        <td  <td class="bg-grey-300" width="10%">
                            <h2>B</h2>
                        </td>
                        <td  <td class="bg-grey-300" width="25%">
                            <h2>OJS21250823-BOT</h2>
                        </td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                    </tr>
                </table>
            </div>
        </li>

        <li>
            <div>
                <table class="table-responsive" width="1200px">
                    <tr>
                        <td class="bg-grey-600" width="15%">
                            <h2>LINE 2</h2>
                        </td>
                        <td class="bg-grey-600" width="10%">
                            <h2>B</h2>
                        </td>
                        <td class="bg-grey-600" width="25%">
                            <h2>OJS21250823</h2>
                        </td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                    </tr>
                </table>
            </div>
        </li>

        <li>
            <div>
                <table class="table-responsive" width="1200px">
                    <tr>
                         <td class="bg-grey-300" width="15%">
                            <h2>LINE 3</h2>
                        </td>
                         <td class="bg-grey-300" width="10%">
                            <h2>B</h2>
                        </td>
                         <td class="bg-grey-300" width="25%">
                            <h2>OJS21250823</h2>
                        </td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                    </tr>
                </table>
            </div>
        </li>

        <li>
            <div>
                <table class="table-responsive" width="1200px">
                    <tr>
                         <td class="bg-grey-600"  width="15%">
                            <h2>LINE 4</h2>
                        </td>
                         <td class="bg-grey-600"  width="10%">
                            <h2>B</h2>
                        </td>
                         <td class="bg-grey-600"  width="25%">
                            <h2>OJS21250823</h2>
                        </td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                        <td class="text-center bg-danger-800" style="color:white; font-size: 15px">ALERT</td>
                    </tr>
                </table>
            </div>
        </li>
    </ul> --}}

	{{-- <ul>
		<li>
			<div>
				1) Lorem ipsum dolor sit amet, porta at, imperdiet id neque. more info
			</div>
		</li>
		<li>
			<div>

				2) Lorem ipsum dolor sit amet, consectetur adipiscing elit. more info
			</div>
		</li>
		<li>
			<div>
				3) Lorem ipsum dolor sit amet more info more info more info more info
			</div>
		</li>
		<li>
			<div>
				4) jugbit.com jquery vticker more info more info more info more info more info
			</div>
		</li>
	</ul> --}}
</div>

<!-- /solid color panels -->
@endsection


@section('page-modal')
@include('preparation_item._tanggal_modal')
@endsection

@section('page-js')
<script>
    /*
* vertical news ticker
* Tadas Juozapaitis ( kasp3rito@gmail.com )
* https://plugins.jquery.com/project/vTicker
*/
(function(a){a.fn.vTicker=function(b){var c={speed:700,pause:4000,showItems:3,animation:"",mousePause:true,isPaused:false,direction:"up",height:0};var b=a.extend(c,b);moveUp=function(g,d,e){if(e.isPaused){return}var f=g.children("ul");var h=f.children("li:first").clone(true);if(e.height>0){d=f.children("li:first").height()}f.animate({top:"-="+d+"px"},e.speed,function(){a(this).children("li:first").remove();a(this).css("top","0px")});if(e.animation=="fade"){f.children("li:first").fadeOut(e.speed);if(e.height==0){f.children("li:eq("+e.showItems+")").hide().fadeIn(e.speed)}}h.appendTo(f)};moveDown=function(g,d,e){if(e.isPaused){return}var f=g.children("ul");var h=f.children("li:last").clone(true);if(e.height>0){d=f.children("li:first").height()}f.css("top","-"+d+"px").prepend(h);f.animate({top:0},e.speed,function(){a(this).children("li:last").remove()});if(e.animation=="fade"){if(e.height==0){f.children("li:eq("+e.showItems+")").fadeOut(e.speed)}f.children("li:first").hide().fadeIn(e.speed)}};return this.each(function(){var f=a(this);var e=0;f.css({overflow:"hidden",position:"relative"}).children("ul").css({position:"absolute",margin:0,padding:0}).children("li").css({margin:0,padding:0});if(b.height==0){f.children("ul").children("li").each(function(){if(a(this).height()>e){e=a(this).height()}});f.children("ul").children("li").each(function(){a(this).height(e)});f.height(e*b.showItems)}else{f.height(b.height)}var d=setInterval(function(){if(b.direction=="up"){moveUp(f,e,b)}else{moveDown(f,e,b)}},b.pause);if(b.mousePause){f.bind("mouseenter",function(){b.isPaused=true}).bind("mouseleave",function(){b.isPaused=false})}})}})(jQuery);

// $(function(){
// 	$('#news-container').vTicker({
// 		speed: 500,
// 		pause: 3000,
// 		animation: 'fade',
// 		mousePause: false,
// 		showItems: 3
// 	});
// });
</script>
<script src="{{ mix('js/preparation_item.js') }}"></script>
@endsection
