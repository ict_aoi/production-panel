<div id="updateTanggalModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
            </div>

				{!!
					Form::open([
						'role'   => 'form',
						'url'    => '#',
						'method' => 'post',
						'class'  => 'form-horizontal',
						'id'     => 'update_date_co'
					])
				!!}
				<div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            @include('form.date', [
                                'field' 		=> 'actual_co_date',
                                'label' 		=> 'Tanggal Actual CO',
                                'label_col'     => 'col-md-5 col-lg-5 col-sm-12',
                                'form_col'      => 'col-md-7 col-lg-7 col-sm-12',
                                'placeholder' 	=> 'yyyy-mm-dd',
                                // 'class' 		=> 'daterange-single',
                                'attributes' 	=> [
                                    'id' 			=> 'up_actual_co_date',
                                    'readonly' 		=> 'readonly',
                                ]
                            ])
                        </div>
                    </div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Submit Form</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
