@extends('layouts.app_dashboard',['active' => 'production_panel'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content text-center">
        {{-- <div class="page-title text-center"> --}}
            <h1 style="font-size: 45px"><i class="icon-grid5 position-center"></i> <span class="text-center text-bold">DASHBOARD CO</span> <i class="icon-grid5 position-center"></i></h1>
            {{-- <br> --}}

			{!! Form::hidden('_factory', $factory,array('id' => '_factory')) !!}
            {!! Form::hidden('_date', $date,array('id' => '_date')) !!}
            {{-- <button type="button" onclick="tanggal()" class="btn btn-default col-xs-12" value="XXX"> </button> --}}
            <input type="text" id="tanggalCO" class="btn btn-default col-xs-12" readonly>
            <br>
        {{-- </div> --}}
    </div>
</div>
@endsection
@section('page-content')
    <br>
    <br>

    <div class="panel-co">

    </div>
    {{-- <div class="row">
        <div class="col-md-3">
            <div class="panel bg-primary">
            </div>
        </div>
    </div> --}}
    <!-- /solid color panels -->
@endsection


@section('page-modal')
	@include('change_over._tanggal_modal')
@endsection

@section('page-js')
    <script src="{{ mix('js/change_over_show.js') }}"></script>
@endsection

