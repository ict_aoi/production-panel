$(document).ready(function () {

    var factory = $('#_factory').val();
    var date = $('#_date').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/dashboard-preparation/' + factory + '/data',
        data: {
            tanggal: date,
            factory: factory
        },
        success: function (response) {
            console.log(response.count > 14);
            $('#news-container').html(response.text);
            if (response.count > 12) {
                $(function () {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 4000,
                        animation: 'fade',
                        mousePause: false,
                        showItems: 12
                    });
                });
            }
            else {
                var show = response.count;
                $(function () {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 4000,
                        animation: 'fade',
                        mousePause: false,
                        showItems: show - 1,
                        pause: 60000
                    });
                });
            }
        }
    });

    setInterval(function () {
        var factory = $('#_factory').val();
        var date = $('#_date').val();

        var today = moment().format('YYYY-MM-DD');
        var today_date = addWeekdays(today, 1).format('YYYY-MM-DD');

        // ADD 0 TO ADD
        var next_date = addWeekdays(date, 1).format('YYYY-MM-DD');
        var check_date = subtractWeekdays(date, 4).format('YYYY-MM-DD');

        // var today_date = moment().format('YYYY-MM-DD');
        // var next_date = moment(date).add('1','day').format('YYYY-MM-DD');

        // var check_date = moment(next_date).subtract('5','days').format('YYYY-MM-DD');
        if (check_date == today_date) {
            // alert("HEY");
            document.location.href = '/dashboard-co-show/' + factory;
        }
        else {
            document.location.href = '/dashboard-preparation-show/' + factory + '/' + next_date;
        }

    }, 60000);

    function addWeekdays(date, days) {
        date = moment(date); // clone
        while (days > 0) {
            date = date.add(1, 'days');
            // decrease "days" only if it's a weekday.
            if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
                days -= 1;
            }
        }
        return date;
    }


    function subtractWeekdays(date, days) {
        date = moment(date); // clone
        while (days > 0) {
            date = date.subtract(1, 'days');
            // decrease "days" only if it's a weekday.
            if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
                days -= 1;
            }
        }
        return date;
    }



    // $('#tanggalPreparation').click(function(){
    //     var tgl = $('#tanggalPreparation').val();
    //     $('#up_plan_date').val(tgl);
    //     $('#updateTanggalModal').modal();
    // });

    // $('input.input-date').datepicker({
    //     format: "yyyy-mm-dd",
    //     autoclose: true,
    //     todayHighlight: true
    // });

    $('#tanggalPreparation').val($('#_date').val());



    // $('#update_plan_co').submit(function (event){
    //     event.preventDefault();

    //     var up_date                = $('#up_plan_date').val();
    //     var factory                = $('#_factory').val();

    //     document.location.href = '/dashboard-preparation-show/'+factory+'/'+up_date;
    // });

});
