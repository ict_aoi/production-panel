$(document).ready( function ()
{

    var factory = $('#_factory').val();
    var date    = $('#_date').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url: '/dashboard-preparation/'+factory+'/data',
        data: {
            tanggal: date,
            factory: factory
        },
        success: function( response ) {
            console.log(response.count>10);
            $('#news-container' ).html(response.text);
            if(response.count>8){
                $(function() {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 4000,
                        animation: 'fade',
                        mousePause: false,
                        showItems: 8
                    });
                });
            }
            else{
                var show = response.count;
                $(function() {
                    $('#news-container').vTicker({
                        speed: 500,
                        pause: 4000,
                        animation: 'fade',
                        // mousePause: false,
                        showItems: show-1,
                        // pause:
                    });
                });

                // $('#news-container').vTicker('remove');
            }
            console.log(show);

        }
    });


    var tgl_awal    = $('#_date').val();

    // setInterval(function(){
    //     var factory = $('#_factory').val();
    //     var date    = $('#_date').val();

    //     console.log(tgl_awal);

    //     var today_date = moment(date).format('YYYY-MM-DD');
    //     var next_date = moment(today_date).add(1, 'day').format('YYYY-MM-DD');

    //     var check_date = moment(today_date).subtract(4, 'days').format('YYYY-MM-DD');
    //     if(check_date == tgl_awal){
    //         // alert("HEY");
    //         document.location.href = '/dashboard-co/'+factory;
    //     }
    //     else{
    //         $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });
    //         $.ajax({
    //             type: 'post',
    //             url: '/dashboard-preparation/'+factory+'/data',
    //             data: {
    //                 tanggal: next_date,
    //                 factory: factory
    //             },
    //             success: function( responseData ) {
    //                 $('.slides' ).html(responseData);
    //                 // $('#updateTanggalModal').modal('hide');
    //                 $('#tanggalPreparation').val(next_date);
    //                 $('#_date').val(next_date);
    //                 $slides = $('.slides');
    //                 $slides.bind('contentchanged', function () {
    //                     animate($slides);
    //                 });
    //                 animate($slides);
    //             }
    //         });
    //     }

    // },120000);


    $('#tanggalPreparation').click(function(){
        var tgl = $('#tanggalPreparation').val();
        $('#up_plan_date').val(tgl);
        $('#updateTanggalModal').modal();
    });

    $('input.input-date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    $('#tanggalPreparation').val($('#_date').val());



    $('#update_plan_co').submit(function (event){
        event.preventDefault();

        var up_date                = $('#up_plan_date').val();
        var factory                = $('#_factory').val();

        document.location.href = '/dashboard-preparation/'+factory+'/'+up_date;
    });

});
