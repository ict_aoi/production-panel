$(document).ready( function ()
{

    var date_co = $('#_date').val();
    $('#tanggalCO').val(date_co);
    $('input.input-date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    var factory =  $('#_factory').val();

    setInterval(function(){

        var factory = $('#_factory').val();
        var next_date = addWeekdays(date_co, 1).format('YYYY-MM-DD');
        document.location.href = '/dashboard-preparation-show/'+factory+'/'+next_date;

    },60000);


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: '/dashboard-co/'+factory+'/data',
        data: {
            tanggal: date_co,
            factory: factory
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            // $('#updateTanggalModal').modal('hide');
            // $('#update_date_co').trigger("reset");
            // console.log(date_co);
           //reload data panel ajax here
            $('.panel-co').html(response.html)
            // $('#productionTable').DataTable().ajax.reload();
            // $("#alert_success").trigger("click", 'Data Berhasil disimpan');
        },
        error: function (response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            // $('#updateTanggalModal').modal();
        }
    });
});


function addWeekdays(date, days) {
    date = moment(date); // clone
    while (days > 0) {
        date = date.add(1, 'days');
        // decrease "days" only if it's a weekday.
        if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
        days -= 1;
        }
    }
    return date;
}
