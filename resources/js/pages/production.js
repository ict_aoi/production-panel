$(document).ready( function ()
{
    $('input.input-date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    var msg = $('#msg').val();
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    var productionTable = $('#productionTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scroller:true,
        // autoWidth:true,
        ajax: {
            type: 'GET',
            url: '/production/data',
        },
        fnCreatedRow: function (row, data, index) {
            var info = productionTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
            $('td', row).eq(1).css('min-width', '150px');
            $('td', row).eq(2).css('min-width', '150px');
            // // $('td', row).eq(10).css('min-width', '200p
            if ( data.status == false ) {
                $(row).addClass('bg-danger-300');
            }
            // $('td', row).eq(3).css('min-width', '150px');
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'uuid', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'status', name: 'status',searchable:false,visible:false,orderable:false},
            {data: 'plan_co_date', name: 'plan_co_date',searchable:true,orderable:true},
            {data: 'actual_co_date', name: 'actual_co_date',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'line', name: 'line',searchable:true,orderable:true},
            {data: 'factory', name: 'factory',searchable:true,orderable:true},
            {data: 'co_category', name: 'co_category',searchable:true,orderable:true},
            {data: 'op_list', name: 'op_list',searchable:true,orderable:true},
            {data: 'layout', name: 'layout',searchable:true,orderable:true},
            {data: 'fabric', name: 'fabric',searchable:true,orderable:true},
            {data: 'machine', name: 'machine',searchable:true,orderable:true},
            {data: 'sample', name: 'sample',searchable:true,orderable:true},
            {data: 'man_power', name: 'man_power',searchable:true,orderable:true},
            {data: 'trimcard', name: 'trimcard',searchable:true,orderable:true},
            {data: 'pattern', name: 'pattern',searchable:true,orderable:true},
            {data: 'critical_process', name: 'critical_process',searchable:true,orderable:true},
            {data: 'mockup', name: 'mockup',searchable:true,orderable:true},
            {data: 'created_at', name: 'created_at',searchable:true,orderable:true},
            {data: 'isactive', name: 'isactive',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:true,orderable:true},
        ]
    });

    var dtable = $('#productionTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#form').submit(function (event){
        event.preventDefault();
        var name = $('#name').val();

        if(!name){
            $("#alert_warning").trigger("click", 'Nama wajib diisi');
            return false
        }

        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if(result){
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        document.location.href = '/account-management/role';
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON.message);

                    }
                });
            }
        });
    });

    // mappings        = JSON.parse($('#mappings').val());
    // var form_status = $('#form_status').val();

    // if(form_status == 'create')
    // {
    //     render();
    // }else
    // {
    //     var url_data_permission = $('#url_data_permission').val();

    //     $('#permissionTable').DataTable().destroy();
    //     $('#permissionTable tbody').empty();
    //     var table = $('#permissionTable').DataTable({
    //         dom: 'Bfrtip',
    //         processing: true,
    //         serverSide: true,
    //         pageLength:10,
    //         scrollY:250,
    //         scroller:true,
    //         destroy:true,
    //         deferRender:true,
    //         bFilter:true,
    //         ajax: {
    //             type: 'GET',
    //             url: url_data_permission,
    //         },
    //         fnCreatedRow: function (row, data, index) {
    //             var info = table.page.info();
    //             var value = index+1+info.start;
    //             $('td', row).eq(0).html(value);
    //         },
    //         columns: [
    //             {data: null, sortable: false, orderable: false, searchable: false},
    //             {data: 'display_name', name: 'display_name',searchable:true,orderable:true},
    //             {data: 'action', name: 'action',searchable:false,orderable:false},
    //         ]
    //     });

    //     var dtable2 = $('#permissionTable').dataTable().api();
    //     $(".dataTables_filter input")
    //         .unbind() // Unbind previous default bindings
    //         .bind("keyup", function (e) { // Bind our desired behavior
    //             if (e.keyCode == 13) {
    //                 // Call the API search function
    //                 dtable2.search(this.value).draw();
    //             }
    //             if (this.value == "") {
    //                 dtable2.search("").draw();
    //             }
    //             return;
    //     });
    //     dtable2.draw();

    //     render();
    // }

    $('#update_production').submit(function (event){
        event.preventDefault();
        // var name = $('#update_name').val();

        // if(!name){
        //     $("#alert_warning").trigger("click", 'Nama wajib diisi');
        //     return false
        // }

        $('#updateProductionModal').modal('hide');
        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?", function (result) {
            if(result){
                $.ajax({
                    type: "PUT",
                    url: $('#update_production').attr('action'),
                    data: $('#update_production').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $('#updateProductionModal').modal('hide');
                        $('#update_production').trigger("reset");
                        $('#productionTable').DataTable().ajax.reload();
                        $('input.input-date').datepicker('setDate', null);
                        $("#alert_success").trigger("click", 'Data Berhasil disimpan');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        $('#updateProductionModal').modal();
                    }
                });
            }
        });
    });
});


function edit(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();

        }
    })
    .done(function (response) {
        $('#update_production').attr('action', response.url_update);
        $('#up_plan_co_date').val(response.plan_co_date);
        $('#up_style').val(response.style);
        $('#up_line').val(response.line);
        $('#up_factory').val(response.factory);
        $('#up_category_co').val(response.category_co);
        $('#up_op_list').val(response.op_list);
        $('#up_layout').val(response.layout);
        $('#up_fabric').val(response.fabric);
        $('#up_machine').val(response.machine);
        $('#up_sample').val(response.sample);
        $('#up_man_power').val(response.man_power);
        $('#up_trimcard').val(response.trimcard);
        $('#up_pattern').val(response.pattern);
        $('#up_critical_process').val(response.critical_process);
        $('#up_mockup').val(response.mockup);
        $('#updateProductionModal').modal();

    });
}

function hapus(url)
{
    bootbox.confirm("Apakah anda yakin akan menghapus data ini ?", function (result) {
        if(result){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "delete",
                url: url,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function (response) {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                }
            }).done(function ($result) {
                $('#productionTable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data Berhasil hapus');
            });
        }
    });
}

function deaktif(url)
{
    bootbox.confirm("Apakah anda yakin akan me-nonaktifkan data ini ?", function (result) {
        if(result){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "delete",
                url: url,
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function (response) {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                }
            }).done(function ($result) {
                $('#productionTable').DataTable().ajax.reload();
                $("#alert_success").trigger("click", 'Data Berhasil Di-nonaktifkan');
            });
        }
    });
}
