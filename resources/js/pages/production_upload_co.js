$(document).ready( function ()
{
    $('#uploadCOTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        pageLength:100,
        deferRender:true
    });
    list = JSON.parse($('#list').val());

    $(function ()
    {
        render();
    });

    function render()
    {
        $('#list').val(JSON.stringify(list));
        var tmpl = $('#list_co').html();
        Mustache.parse(tmpl);
        var data = { item: list };
        var html = Mustache.render(tmpl, data);
        $('#tbody-co').html(html);
    }

    $('#upload_button').on('click', function ()
    {
        $('#upload_file').trigger('click');
    });

    $('#upload_file').on('change', function ()
    {
        //$('#upload_file_allocation').submit();
        $.ajax({
            type: "post",
            url: $('#upload_file_co').attr('action'),
            data: new FormData(document.getElementById("upload_file_co")),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#alert_info_2").trigger("click", 'Upload successfully.');
                console.log(response);
                list = response;

            },
            error: function (response) {
                $.unblockUI();
                $('#upload_file_co').trigger('reset');
                if (response['status'] == 500)
                    $("#alert_error").trigger("click", 'Please Contact ICT.');

                if (response['status'] == 422)
                    $("#alert_error").trigger("click", response.responseJSON);

            }
        })
        .done(function () {
            $('#upload_file_co').trigger('reset');
            render();
        });

    })
});
