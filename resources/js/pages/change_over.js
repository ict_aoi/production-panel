$('#tanggalCO').click(function(){
    var tgl = $('#tanggalCO').val();

    var tgl = $('#tanggalCO').val();
    $('#up_actual_co_date').val(tgl);
    $('#updateTanggalModal').modal();
});

$(document).ready( function ()
{

    var date_co = $('#_date').val();
    $('#tanggalCO').val(date_co);
    $('input.input-date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    var factory =  $('#_factory').val();

    // setInterval(function(){

    //     var factory = $('#_factory').val();
    //     document.location.href = '/dashboard-preparation/'+factory;

    // },120000);


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: '/dashboard-co/'+factory+'/data',
        data: {
            tanggal: date_co,
            factory: factory
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            // $('#updateTanggalModal').modal('hide');
            // $('#update_date_co').trigger("reset");
            // console.log(date_co);
           //reload data panel ajax here
            $('.panel-co').html(response.html)
            // $('#productionTable').DataTable().ajax.reload();
            // $("#alert_success").trigger("click", 'Data Berhasil disimpan');
        },
        error: function (response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            // $('#updateTanggalModal').modal();
        }
    });
});

$('#update_date_co').submit(function (event){
    event.preventDefault();

    var factory = $('#_factory').val();
    var up_date = $('#up_actual_co_date').val();

    $.ajax({
        type: "post",
        url: '/dashboard-co/'+factory+'/data',
        data: {
            tanggal: up_date,
            factory: factory
        },
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            // $('#updateTanggalModal').modal('hide');
            // $('#update_date_co').trigger("reset");

            $('#updateTanggalModal').modal('hide');
            $('#tanggalCO').val(up_date);

            // $("#alert_success").trigger("click", 'Data Yang Tampil Berdasar Tanggal Pilihan');
            $('#update_date_co').trigger("reset");
           //reload data panel ajax here
            $('.panel-co').html(response.html)

        },
        error: function (response) {
            $.unblockUI();
            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
            $('#updateTanggalModal').modal();
        }
    });
});
