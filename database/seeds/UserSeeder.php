<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run()
    {
        $roles  = Role::Select('id')->get();

        $user                    = new User();
        $user->name              = 'Admin ICT BBI';
        $user->nik               = '11111111';
        $user->email             = 'admin_ict@bbi.co.id';
        $user->email_verified_at = carbon::now();
        $user->factory_id        = '2';
        $user->password          = bcrypt('password1');
        $user->is_super_admin    = true;

        if($user->save())
            $user->attachRoles($roles);
    }
}
