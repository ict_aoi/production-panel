<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoringProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_productions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('plan_co_date')->nullable();
            $table->date('actual_co_date')->nullable();
            $table->string('style')->nullable();
            $table->string('line')->nullable();
            $table->string('factory')->nullable();
            $table->string('co_category')->nullable();

            $table->date('op_list')->nullable();
            $table->date('layout')->nullable();
            $table->date('fabric')->nullable();
            $table->date('machine')->nullable();
            $table->date('sample')->nullable();
            $table->date('man_power')->nullable();
            $table->date('trimcard')->nullable();
            $table->date('pattern')->nullable();
            $table->date('critical_process')->nullable();
            $table->date('mockup')->nullable();

            $table->string('created_by')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_productions');
    }
}
